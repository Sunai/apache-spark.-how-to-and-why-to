How to install Apache Spark 3.0 on your local device
----------------------------------------------------
1. Download and install a current JDK from 

https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html

(to check your Java version, type $ java -version)

2. Download and install Spark 3.0 with the packagebuilt for Apache Hadoop 3.2 and later from 

http://spark.apache.org/downloads.html 

3. Unzip the package from downloads and add to PATH (user home) with

$ cd downloads
$ tar -xvf spark-3.0.0-preview2-bin-hadoop3.2.tgz -C ~/

optional:
4. Set a symbolic link to the Spark 3.0 folder with

$ ln -s s spark-3.0.0-preview2-bin-hadoop3.2.tgz spark

5. Run Spark 3.0 with

$ bin/spark-shell

6. Download Stack Overflow Annual Developer Survey 2019 from

https://insights.stackoverflow.com/survey/?utm_source=so-owned&utm_medium=blog&utm_campaign=dev-survey-2017&utm_content=blog-link&utm_term=data&__hstc=188987252.dbb0b2e05ab1172a6807c445777c3df7.1580762464027.1580762464027.1580770905805.2&__hssc=188987252.1.1580770905805&__hsfp=3829167435